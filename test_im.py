import utils.datasets as d
import utils.models as m
import utils.samplers as s

import torch
from numpy.random import permutation

from utils.misc import set_seed

set_seed(1998)

if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")

datai, modeli1, optimizeri1, scheduleri1 = m.init_imbalance_problem("data/", device)
datai, modeli2, optimizeri2, scheduleri2 = m.init_imbalance_problem("data/", device)
dataac, modelac, optimizerac, schedulerac = m.init_allen_problem("data/AC.mat", device)

modeli2.load_state_dict(modeli1.state_dict().copy())

N = 100
lds = s.BreedSampler(datai.domain, 0.005, 0.15, 0.7, 4, N)
r3s = s.R3Sampler(tuple(datai.domain[0]), tuple(datai.domain[1]), N)
r3s.x, r3s.t = lds.x[:,0:1].clone(), lds.x[:,1:2].clone()

print(lds.R)

batchsize = 10
nbatches = N // batchsize

print("LDS current:\n", lds.current())
print("R3S current:\n", r3s.current())
for i in range(10):
    print(f"Iteration {i}")
    out_gd_lds = torch.tensor(datai.run_simulation(lds.current().detach().numpy())).float()
    print("simulation finished")
    perm = permutation(N)
    run_loss = 0
    for batch in range(nbatches):
        perm_batch = perm[batch*batchsize:(batch + 1)*batchsize]
        optimizeri1.zero_grad()
        hat_lds = modeli1(lds.current()[perm_batch])
        lps1, loss1 = modeli1.compute_loss(hat_lds, out_gd_lds[perm_batch])
        run_loss += loss1.item()
        loss1.backward()
        optimizeri1.step()
    

    out_gd_r3s = torch.tensor(datai.run_simulation(r3s.current().detach().numpy())).float()
    print("simulation finished")
    hat_r3s = modeli2(r3s.current())
    optimizeri2.zero_grad()
    lps2, loss2 = modeli2.compute_loss(hat_r3s, out_gd_r3s)
    loss2.backward()
    optimizeri2.step()

    print("LDS loss =", run_loss / nbatches)
    print("R3S loss =", loss2.item())

    with torch.no_grad():
        lps1, _ = modeli1.compute_loss(modeli1(lds.current()), out_gd_lds)
        lps2, _ = modeli2.compute_loss(modeli2(r3s.current()), out_gd_r3s)

        lds.update(torch.abs(lps1).detach())
        r3s.update(torch.abs(lps2).detach())
        print("--------")
        print("R=", lds.R[lds.R_i])
        print("LDS: val loss =", ((modeli1(datai.val_inputs) - datai.val_outputs)**2).mean())
        print("R3S: val loss =", ((modeli2(datai.val_inputs) - datai.val_outputs)**2).mean())
        print("--------")

