style ={
        "font.family" : 'serif',
        "font.size"   : 12 * 1.5,
        "ytick.labelsize" : 8*1.5,
        "xtick.labelsize" : 8*1.5,
        "axes.labelsize" : 14 * 1.5,
        "axes.titlesize" : 14 * 1.5,
        "legend.fontsize": 8 * 1.5
        }
palette = 'seaborn-v0_8-paper'
