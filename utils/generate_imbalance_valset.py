import json
import os
import random

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import qmc

from utils.gdsimulator import imbalance_gd, grad_mix_bell_curve, imbalance_config

seed = 1998
set_seed(seed)

datapath = "../data/"
np.savez(os.path.join(datapath, "config.npz"),
        full_interval=imbalance_config["full_interval"],
        **imbalance_config["surface_kwargs"]
        )

interval = imbalance_config["full_interval"]
N = 10000

sampler = qmc.Halton(d=interval.shape[0], seed=seed)
X = sampler.random(n=N)
X = X * (interval[:,1] - interval[:,0]) + interval[:,0]
inputs, outputs = imbalance_gd(samples=X)

np.save(os.path.join(datapath, "inputs.npy"), inputs)
np.save(os.path.join(datapath, "outputs.npy"), outputs)

hard_val_set = np.zeros((N, interval.shape[0]))
l = 0
while l < N:
    # sample 10000 points
    X = np.random.uniform(
        low=interval[:,0],
        high=interval[:,1],
        size=(N-l, interval.shape[0])
        )
    # calculate gradients
    gradients = grad_mix_bell_curve(X, imbalance_config["surface_kwargs"])
    gradients = np.sqrt(np.power(gradients, 2).sum(-1))
    # take N points where gradient is small (percentile?)
    filterr = gradients < 4e-3
    iob = filter.sum()
    hard_val_set[l:l+iob] = X[filterr]
    l += iob

inputs, outputs = imbalance_gd(samples=hard_val_set)

np.save(os.path.join(datapath, "hard_inputs.npy"), inputs)
np.save(os.path.join(datapath, "hard_outputs.npy"), outputs)
