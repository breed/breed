import numpy as np

# copied code for generating two imbalanced pits dataset

# choosing symmetric interval by [x_max, y_max]
interval = np.array([2, 2])
# choosing interval by [[x_min, x_max], [y_min, y_max]]
full_interval = np.array([[-2, 2], [-2, 2]])

centers = [x.tolist() for x in
    (
        -interval / 2 + interval / 4,                      # bell #1
        interval / 2 + interval / 4,                       # bell #2
        0.5 * interval * np.array([1, -1]) + interval / 4, # bell #3 and 
        0.5 * interval * np.array([-1, 1]) + interval / 4  # bell #4 are smoothing out flat corners
    )
]
wides = [x.tolist() for x in
    (
        interval / 5,
        interval / 250,
        interval / 4,
        interval / 4
    )
]
weights = [
    0.895,
    0.005,
    0.05,
    0.05
]

surface_kwargs = {
    'centers': centers, 
    'wides': wides,
    'weights' : weights
    }

