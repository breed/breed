import os
import argparse
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from misc import weighted_avg_and_std
from plot_params import style, palette
from matplotlib import colormaps

cmap = colormaps["inferno"]

plt.style.use(palette)
plt.rcParams.update(style)

parser = argparse.ArgumentParser()
parser.add_argument("path", help="Path to directory containing train_stats.csv. Can be a directory with directories of snake, then the plots are groupped.")
parser.add_argument("--mean", action="store_true", default=False, help="Flag to average over random seed.")
args = parser.parse_args()

os.makedirs(os.path.join(args.path, "plots"), exist_ok=True)
problem = args.path.split("/")[-2]
listdir = os.listdir(args.path)
if "train_stats.csv" in listdir:
    
    df = pd.read_csv(f"{args.path}/train_stats.csv")
    fig, axs = plt.subplot_mosaic([['train_loss', 'test_loss', 'misc']],
            layout='constrained', figsize=(4.8, 9.6))
    for label, ax in axs.items():
        ax.set_title(label, fontstyle='italic')
        ax.plot(df[label])
    print("saving to:", f"{args.path}/plots/train_stats.pdf")
    plt.savefig(f"{args.path}/plots/train_stats.pdf")
else:
    all_arrays = {
            "r3": {
                "Validation set": [],
                "Hard validation set": [],
                "style": {
                    "linestyle": ":",
                    "linewidth": 2.5,
                    "color" : cmap(0.4)
                    }
                },
            "breed": {
                "Validation set": [],
                "Hard validation set": [],
                "style": {
                    "linestyle": "-",
                    "linewidth": 2.5,
                    "color": cmap(0.8)
                    }
                },
            "uniform": {
                "Validation set": [],
                "Hard validation set": [],
                "style": {
                    "linestyle": "--",
                    "linewidth": 2,
                    "color": cmap(0.0) 
                    }
                },
            }

    for adir in listdir:
        if adir == "plots": continue
        method = adir.split("_")[-1]
        try:
            df_adir = pd.read_csv(os.path.join(args.path, adir, "train_stats.csv"))
            all_arrays[method]["Validation set"].append(df_adir["test_loss"].values.reshape(1, -1))
        except:
            pass
        if problem == "imbalance":
            all_arrays[method]["Hard validation set"].append(df_adir["hard_test_loss"].values.reshape(1, -1))

    for method in all_arrays.keys():
        over = ["Validation set"]
        if problem == "imbalance":
            over.append("Hard validation set")
        for tt in over:
            final = all_arrays[method][tt][0]
            for x in all_arrays[method][tt][1:]:
                fN = final.shape[1]
                xN = x.shape[1] 
                N = abs(xN - fN)
                if fN > xN:
                    x = np.pad(x, ((0, 0), (0, N)), "constant", constant_values=None)
                elif fN < xN:
                    final = np.pad(final, ((0, 0), (0, N)), "constant", constant_values=None)
                final = np.vstack([final, x])
            all_arrays[method][tt] = final
mosaic = [["Validation set"]]
figsize = (6, 4)
if problem == "imbalance":
    mosaic[0].append("Hard validation set")
    figsize = (12, 4)
fig, axs = plt.subplot_mosaic(mosaic,
        layout='constrained', figsize=figsize)
for label, ax in axs.items():
    ax.set_title(label)
    for method in ["uniform", "r3", "breed"]:
        step = 100 if problem == "allencahn" else 1
        t = range(0, all_arrays[method][label].shape[1], step)
        if args.mean:
            mu, std = weighted_avg_and_std(all_arrays[method][label][:, t], None, 0)
            ax.plot(t, mu, label=method.capitalize(), **all_arrays[method]["style"])
            ax.fill_between(t, mu + std, mu - std, alpha=0.3, color=all_arrays[method]["style"]["color"])
        else:
            ax.plot(t, all_arrays[method][label][:, t].T, **all_arrays[method]["style"])
            ax.plot([],[], label=method.capitalize(), **all_arrays[method]["style"])
        if problem == "imbalance" and label.startswith("Valid"):
            ax.set_yscale("log")
        ax.set_xlabel("Iteration")
        if problem == "imbalance":
            ax.set_ylabel("Huber Loss")
            if label.startswith("Valid"):
                ax.set_ylabel("Huber Loss (Log)")
        else:
            ax.set_ylabel("Relative $L^2$ error")
        ax.set_xlim(-0.5, t[-1] + 1)
        ax.legend()
        ax.grid(True)
print("saving to:", f"{args.path}plots/train_stats{'_mean' if args.mean else '_seeds'}.pdf")
plt.savefig(os.path.join(args.path, "plots", f"train_stats{'_mean' if args.mean else '_seeds'}.pdf"))


