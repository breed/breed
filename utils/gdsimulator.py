import numpy as np
import matplotlib.pyplot as plt
import ray
import torch

from scipy.stats import multivariate_normal
from tqdm import tqdm


def bell_curve(x, center=0, width=1):
    """
    x: nparray (N), (N,N,2), (N, N, N, 3) ...
    center: [m], [m, m], [m, m, m]
    """
    mean = np.asarray([center]).ravel()
    width = np.asarray([width]).ravel()
    cov = np.diag(width)
    return - multivariate_normal(mean, cov).pdf(x)

def mix_bell_curves(x, centers, widths, weights=None):
    """
    centers and widths are lists of same length N, each contatins centers and widths
    of N bell curves to mix
    """
    N = len(centers)
    assert N == len(widths)
    if weights is None:
        weights = np.full(N, 1 / N)
    else:
        assert np.sum(weights) == 1
    mix = bell_curve(x, centers[0], widths[0]) * weights[0]
    for i in range(1, N):
        mix += bell_curve(x, centers[i], widths[i]) * weights[i]
    return mix # / N # optional

def grad_bell_curve(x, center, width):
    dxdy = - bell_curve(x, center, width)[..., None] * (x - center) / width
    return dxdy

def grad_mix_bell_curve(x, centers, widths, weights):
    N = len(centers)
    assert N == len(widths)
    dxdy = grad_bell_curve(x, centers[0], widths[0]) * weights[0]
    for i in range(1, N):
        dxdy += grad_bell_curve(x, centers[i], widths[i]) * weights[i]
    return dxdy

def compute_grad(sample, target, model, criterion):
    sample = sample.unsqueeze(0)  # prepend batch dimension for processing
    target = target.unsqueeze(0)

    prediction = model(sample)
    loss = criterion(prediction, target)

    gradients = torch.autograd.grad(loss, list(model.parameters()))

    return sum([gr.abs().sum().item() for gr in gradients])


def get_stats_per_sample_grad(data, targets, model, criterion):
    """ 
    returns numpy array of same length as given argument data
    computes per sample a sum of absolute gradient dL/dw values
    """
    if not torch.is_tensor(data):
        model_type = next(model.parameters()).dtype
        data, targets = torch.from_numpy(data).type(model_type), torch.from_numpy(targets).type(model_type)

    per_sample_grads = np.array([compute_grad(data[i], targets[i], model, criterion) for i in range(len(data))])

    per_sample_grads -= per_sample_grads.min()
    per_sample_grads /= per_sample_grads.max()
    return per_sample_grads


@ray.remote
def gradient_descent(
    f,
    df,
    start,
    lr=0.1,
    iterations=10000,
    eps=1e-9,
    return_trajectory=False,
    return_length=False,
    verbose=False,
    **kwargs,
):
    """
    Arguments:
    
    f and df:
        functions which take X : np.array of size [N, D] and **kwargs
        and outputs Y (dx...dy) of size [N, D]

    start:
        nparray [x, y] floats - the start point of GD

    lr, iterations, eps:
        arguments of gradient descent algorithm

    return_trajectory:
        True or False whether to return trajectory of descent or just start and end points

    return_length
        True or False whether to return number of iterations of descent

    verbose:
        print debugging

    **kwargs or f and df

    Returns:
    ndarray [N, D] of outputs of GD on __samples__ [N,D]
    """

    D = len(start)

    if return_trajectory:
        thetas = np.empty((iterations, D))
        thetas[0] = start
    else:
        next_theta = start

    it = 0
    dfx = np.inf
    while dfx > eps and it < iterations:
        current_theta = thetas[it] if return_trajectory else next_theta
        dfxdfy = df(current_theta, **kwargs)
        next_theta = current_theta - lr * dfxdfy

        previous_f = f(current_theta, **kwargs)
        current_f = f(next_theta, **kwargs)
        dfx = abs(previous_f - current_f)
        if return_trajectory:
            thetas[it + 1] = next_theta
        if verbose:
            print("it", it, "grad", dfxdfy)
            print("next theta:", next_theta)
        it += 1
    if verbose:
        print(
            f"the last change was {dfx} on iteration {it}"  # , theta = ({thetas[-1][0]:5f}, {thetas[-1][1]:5f})"
        )
#    if return_trajectory:
#        return thetas[:it]
#    else:
#        if return_length:
#            return (next_theta, it)
#        else:
    return next_theta


def gradient_descent_with_repeat(
    f,
    df,
    samples,
    interval,
    lr=0.1,
    iterations=10000,
    eps=1e-9,
    return_trajectory=False,
    return_length=False,
    verbose=False,
    **kwargs,
):
    """Runs gradient descent (GD) simulator:
        (1) on uniformly sampled __samples__ (int) times start points
        (2) on __samples__ (ndarray) as inputs

    Arguments:

    f and df:
        functions which take X ndarray of size [N, D] and **kwargs
        and output ndarray Y (dx...dy) of size [N, D]

    interval: np.ndarray [D, 2]
        [[xmin, xmax], [ymin, ymax], ...] - the interval for GD
    
    samples:
        (1) int which indicates number of start points to create
        OR
        (2) ndarray [N, D] floats - the start points of GD

    lr, iterations, eps:
        arguments of GD algorithm

    return_trajectory: bool
        whether to return trajectory of GD or just start and end points

    return_length: bool
        whether to return number of iterations of GD
        
    verbose: bool
        print debugging
        
    **kwargs of f and df
        
    Returns:
    (1) tuple ndarrays ([N, D], [N, D]) of uniformly samples inputs and outputs of GD
    (2) ndarray [N, D] of outputs of GD on samples
    """

    if return_trajectory or return_length: raise NotImplementedError("sorry")
    D = len(interval)
    isuniform = isinstance(samples, int)
    if isuniform:
        N = samples
        samples = np.random.uniform(low=interval[:,0], high=interval[:,1], size=(N, D))
    else:
        N = len(samples)
    
    future = [gradient_descent.remote(
            f=f,
            df=df,
            start=ray.put(samples[i]),
            lr=lr,
            iterations=iterations,
            eps=eps,
            return_trajectory=return_trajectory,
            return_length=return_length,
            verbose=verbose,
            **kwargs,
        ) for i in range(N)]
    
    outputs = np.array(ray.get(future))
    if isuniform:
        return (samples, outputs)
    else:
        return outputs


imbalance_config = {
        "surface_kwargs" : {
                "centers": np.array([[-0.5, -0.5], [1.5, 1.5], [1.5, -0.5], [-0.5, 1.5]]),
                "widths": np.array([[0.4, 0.4], [0.008, 0.008], [0.5, 0.5], [0.5, 0.5]]),
                "weights": np.array([0.895, 0.005, 0.05, 0.05])
                },
        "full_interval": np.array([[-2, 2],[-2,2]])
        }

def imbalance_gd(samples: np.ndarray):
    """Little wrapper to run simulation of Imbalanced Pits Surface problem"""
    return gradient_descent_with_repeat(
            f=mix_bell_curves,
            df=grad_mix_bell_curve,
            samples=samples,
            interval=imbalance_config["full_interval"],
            **imbalance_config["surface_kwargs"]
            )
