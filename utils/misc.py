import os

import numpy as np
import math
import random
import torch

def set_seed(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)

def linlog_spaced_int(start, end, N):
    logspaced = np.logspace(np.log10(start), np.log10(end), N)
    logspaced = np.array([round(x) for x in logspaced]) # int is wrongly casted
    rate = (end / start) ** (1 / N)
    # k-th number is a number s.t. distance with k+1 is >= 1
    k = round(- np.log((rate - 1) * start) / np.log(rate) + 1)
    # and to have enough points to get N we prolong linear spaced
    pivot = 2 * k - logspaced[k]
    part1 = np.arange(start, pivot, dtype=int)
    part2 = logspaced[pivot - 1:]
    full = np.hstack([part1, part2])
    return full

def weighted_avg_and_std(values, weights=None, axis=None):
    """
    Return the weighted average and standard deviation.

    values, weights -- NumPy ndarrays with the same shape.
    """
    average = np.average(values, weights=weights, axis=axis)
    # Fast and numerically precise:
    variance = np.average((values-average)**2, weights=weights, axis=axis)
    return (average, np.sqrt(variance))
