import numpy as np
import matplotlib.pyplot as plt
import scipy.io
import torch

import utils.gdsimulator as gd

class ImbalanceData:
    """
    Objects allows to access validation dataset (through fileds) and run simulation for arbitrary input
    """
    def __init__(self, datapath, device="cpu"):
        self.surface_kwargs = gd.imbalance_config["surface_kwargs"]
        self.domain = gd.imbalance_config["full_interval"]
        self.val_inputs = torch.from_numpy(np.load(f"{datapath}/inputs.npy")).float().to(device)
        self.val_outputs = torch.from_numpy(np.load(f"{datapath}/outputs.npy")).float().to(device)
        self.hard_val_inputs = torch.from_numpy(np.load(f"{datapath}/hard_inputs.npy")).float().to(device)
        self.hard_val_outputs = torch.from_numpy(np.load(f"{datapath}/hard_outputs.npy")).float().to(device)

    def run_simulation(self, inputs):
        """Given inputs for GD simulation, returns its outputs
        """
        return gd.imbalance_gd(inputs)

    def plot_reference(self, path):
        return

class AllenCahnData:
    """
    Provides access to refernce (val) solution on collocation points (regular grid on domain),
    including initial conditions for loss calculation

    Does not provide simulation run
    """
    def __init__(self, datapath="AC.mat", device="cpu"):
        """
        A little object to load and access data for AllenCahn problem
        u_t + 5 * u**3 - 5 * u - nu * u_xx 
        """
        super().__init__()
        self.data = scipy.io.loadmat(datapath)
        # numpy
        self.usol = self.data["uu"] # shape [512, 201] X * T
        self.usol_norm = np.linalg.norm(self.usol)
        self.t_star = self.data["tt"][0] # [201, ]
        self.x_star = self.data["x"][0] # [512, ]
        self.TT, self.XX = np.meshgrid(self.t_star, self.x_star)
        
        self.domain = np.array([[-1., 1.], [0., 1.]])
        
        self.t_lim = (0., 1.)
        self.x_lim = (-1., 1.)
        
        # torch
        self.X_star = torch.tensor(self.XX, device=device).float().flatten().unsqueeze(1)
        self.T_star = torch.tensor(self.TT, device=device).float().flatten().unsqueeze(1)
        self.T_0 = torch.zeros((self.x_star.shape[0], 1), device=device, requires_grad=True).float()
        self.X_0 = torch.tensor(self.x_star.reshape(-1, 1), device=device, requires_grad=True).float()
        self.Y_0 = torch.tensor(self.usol[:, 0:1], device=device, requires_grad=True).float().flatten()

    def plot_reference(self, path):
        plt.figure(figsize=(5, 4), dpi=150)
        plt.pcolor(self.TT, self.XX, self.usol, cmap="jet")
        plt.colorbar()
        plt.xlabel("$t$")
        plt.ylabel("$x$")
        plt.savefig("{path}/reference_solution.pdf")
        plt.close()
