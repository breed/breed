import torch
import torch.nn as nn
import numpy as np
from scipy.stats import multivariate_normal

def uniform_tensor(self, N, lim, device):
    return torch.zeros(N, 1, dtype=torch.float32, device=device).uniform_(*lim)


class BaseSampler:
    def __init__(self, domain, N=1000):
        self.N = N
        self.domain = torch.from_numpy(domain).float() if isinstance(domain, np.ndarray) else domain
        self.ndim = self.domain.ndim


class FIXSampler(BaseSampler):
    def __init__(self, domain: torch.Tensor, N=1000):
        super().__init__(domain, N)
        self.x = torch.distributions.uniform.Uniform(self.domain[:,0], self.domain[:,1]).sample((self.N,))

    def update(self, loss: torch.Tensor):
        return self.x


class UNISampler(BaseSampler):
    def __init__(self, domain: torch.Tensor, N=1000):
        super().__init__(domain, N)
        self.uniform = torch.distributions.uniform.Uniform(self.domain[:,0], self.domain[:,1])
        self.x = self.uniform.sample((self.N,))

    def update(self, loss: torch.Tensor):
        self.x = self.uniform.sample((self.N,))
        return self.x

    def current(self):
        return self.x

    def get_misc(self):
        return 0

class BreedSampler(UNISampler):
    """Class for a sampler proposed by authors

    Attributes:
    
    N: int
        Number of points to sample each call
    domain: torch.tensor
        The interval for sampling
    ndim: int
    sigma: float
        The parameter responsible for neighbourhood widths
    R: np.array of floats
        The array of R-values according to initialization parameters
    R_i: int
        Counter of epochs and index for R values array
    distribution: torch.tensor
        The last loss-based distribution
    oob_count_global: int
        Number of points which were sampled out of domain and needed to be resampled.
        Can help to change sigma value.
    x: torch.tensor
        The last sampled points tensor
    uniform:
        torch distribution function, method .sample(shape) gives uniform tensor
    Methods:
    
    update
        Given loss updates distribution and x
    current
        Returns current x
    """
    def __init__(self, domain, sigma, start=0.15, end=0.75, breakpoint=10, N=1000):
        """
        Args:
        
        domain: torch.Tensor [D, 2]
        sigma: float
            The covariance value for Gaussian neighbourhoods
        start, end: float
            The first and last R value to create the R values series
        breakpoint: int
            The value specifies R value scenario, how many values are 
            linearly increasing from start to end, after which R values are constant (=end)
        N: int
            The size of iteration training set
        """
        super().__init__(domain, N)

        self.autosigma()
        if sigma is not None and sigma < self.sigma:
            self.sigma = sigma
        else:
            print(f"WARNING: Given value of sigma {sigma} is too big, it is updated to {self.sigma}")        
        self.R = np.linspace(start, end, breakpoint, endpoint=True)
        self.R_i = -1

    def autosigma(self): # sigma can't be too big
        self.sigma = (self.domain[:,1] - self.domain[:,0]).min() / 10 # (should it be 100?)

    def update(self, loss):
        """Given loss, updates training set (field self.x) and return self.x in shuffled manner
        Provides also counter of out of bound points by self.oob_count_global
        Distribution on parental points accessed by self.distribution
        """
        if len(loss) != len(self.x):
            raise RuntimeError("Input loss mismatches dimensions with x")

        if self.R_i < len(self.R) - 1:
            self.R_i += 1

        R = self.R[self.R_i]
        loss = loss.numpy()
        current = self.current().numpy()
        self.distribution = (loss / loss.sum()).flatten()
        parental_idx = np.random.choice(np.arange(self.N), size=self.N, p=self.distribution.ravel())

        new_sample_batch = np.empty((self.N, self.ndim))
        count_uniform = 0
        self.oob_count_global = 0
        for i, idx in enumerate(parental_idx):
            if np.random.uniform(0, 1) < R:
                rv = multivariate_normal.rvs(mean=current[idx], cov=self.sigma)
                while (abs(rv) > 2).any():
                    self.oob_count_global += 1
                    rv = multivariate_normal.rvs(mean=current[idx], cov=self.sigma)
            else:
                count_uniform += 1
                rv = np.random.uniform(self.domain[:, 0], self.domain[:, 1], (1, 2))
            new_sample_batch[i] = rv
        print("Given R:", R, "real ratio:", 1 - count_uniform / self.N)
        self.x = new_sample_batch
        return self.x

    def current(self):
        if isinstance(self.x, np.ndarray):
            return torch.from_numpy(self.x).float()
        else:
            return self.x

    def get_misc(self):
        return self.oob_count_global

class R3Sampler(nn.Module):
    def __init__(self, x_lim: tuple, t_lim: tuple, N=1000, device=None):
        super(R3Sampler, self).__init__() 
        self.N = N
        self.x_lim = x_lim
        self.t_lim = t_lim
        self.n_retained = []
        self.device = device if device is not None else torch.device("cpu")
        self.uniform = torch.distributions.uniform.Uniform(
                torch.tensor([x_lim[0], t_lim[0]]).float(),
                torch.tensor([x_lim[1], t_lim[1]]).float())
        xt = self.uniform.sample((self.N, ))
        self.x = xt[:,0:1]
        self.t = xt[:,1:2]
    
    def update(self, loss: torch.Tensor):
        if len(loss) != len(self.x) or len(loss) != len(self.t):
            raise RuntimeError("Input loss mismatches dimension with x, or t.")
        
        x_old, t_old, x_new, t_new = self.get_old_new(loss)
        
        self.x = torch.cat((x_old, x_new), dim=0)
        self.t = torch.cat((t_old, t_new), dim=0)
        return self.x, self.t
    
    def get_old_new(self, loss: torch.Tensor):
        mask = loss > loss.mean()
        x_old = self.x[mask].reshape(-1, 1) 
        t_old = self.t[mask].reshape(-1, 1)
        self.n_retained.append(torch.sum(mask).item())
        xt = self.uniform.sample((self.N - self.n_retained[-1], ))
        #x_new = torch.zeros(self.N - self.n_retained[-1], 1, dtype=torch.float32, device=self.device).uniform_(*self.x_lim)
        #t_new = torch.zeros(self.N - self.n_retained[-1], 1, dtype=torch.float32, device=self.device).uniform_(*self.t_lim)
        return x_old, t_old, xt[:, 0:1], xt[:, 1:2]
    
    def current(self):
        return torch.cat((self.x, self.t), dim=1)

    def get_misc(self):
        return self.n_retained[-1]

def init_sampler_from_args(args, domain, size, device):
    """wrapper function to create sampler in run.py script from arguments (clearer view)
    """
    whichsampler = {
        "breed": (BreedSampler, (domain, args.sigma, args.startval,
                                  args.endval, args.constafter, size)),
        "r3" : (R3Sampler, (domain[0], domain[1], size, device)),
        "uniform" : (UNISampler, (domain, size))
        }
    sampler = whichsampler[args.sampler][0](*whichsampler[args.sampler][1])
    return sampler
