from collections import OrderedDict

import torch
import numpy as np
from torch import nn

from utils.datasets import ImbalanceData, AllenCahnData


class DNNImbalance(nn.Module):
    def __init__(self, data, inp=2, out=2, hidden=[64, 32, 16, 8], activation="leaky", max_iter=100, iter_size=100, batch_size=10):
        """
        This class is a model for solving benchmark problem "Imbalanced pits",
        it's default parameters are exactly the parameters for 
        the model that was found (for uniform sampling) to be the best by authors.

        data: object of class ImbalanceData
        """
        super().__init__()
        self.data = data
        self.hidden = hidden
        self.loss_pers = nn.HuberLoss(reduction="none")
        self.loss = nn.HuberLoss(reduction="mean")
        self.act_layer = nn.LeakyReLU
        self.max_iter = max_iter
        self.iter_size = iter_size
        self.batch_size = batch_size

        self.first_layer = [
                nn.Linear(inp, self.hidden[0]), 
                nn.ReLU()
                ]
        self.layers = []
        for i in range(len(self.hidden) - 2):
            self.layers += [
                    nn.Linear(self.hidden[i], self.hidden[i+1]),
                    self.act_layer()
                    ]

        self.last_layer = [
                nn.Linear(self.hidden[-2], self.hidden[-1]),
                nn.ReLU(), 
                nn.Linear(self.hidden[-1], out)
                ]
        self.model = nn.Sequential(*self.first_layer, *self.layers, *self.last_layer)
        print("Model for imbalanced problem is initialized:\n", self.model)

    def forward(self, x):
        x = self.model(x)
        return x

    def compute_loss(self, hat_y, y):
        loss_per_sample = self.loss_pers(hat_y, y).sum(-1)
        loss = self.loss(hat_y, y)
        return loss_per_sample, loss

    def evaluate(self):
        return None

def init_imbalance_problem(datapath, device):
    """
    The function returns objects: data, model, optimizer, and scheduler,
    where parameters and choices of algorithms are exactly
    the same as ones used for experiments by authors
    """
    data = ImbalanceData(datapath, device)
    model = DNNImbalance(data, max_iter=100, iter_size=1000, batch_size=50).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-2, weight_decay=5e-5)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, patience=5)
    return data, model, optimizer, scheduler

class DNNAllenCahn(nn.Module):
    """
    The code copied from https://github.com/arkadaw9/r3_sampling_icml2023/blob/main/Allen_Cahn/model.py

    For simplification, the model is simplified up to parts of the code
    that were used in Allen Cahn experiments by authors [Daw et al. 2023]
    """
    def __init__(self, data, layers=[22, 128, 128, 128, 128, 1], activation="tanh", L=2.0, M=10, max_iter=100, iter_size=100, batch_size=10):
        """
        data: class object AllenCahnData
        """
        super().__init__()

        self.data = data
        # Initial Condition tensors

        self.L = L
        self.M = M
        print(f"Initializing a default MLP with L:{L}, M:{M}, layers: {layers}")
        
        self.max_iter = max_iter
        self.iter_size = iter_size
        self.batch_size = batch_size

        # parameters
        self.depth = len(layers) - 1
        self.activation = torch.nn.Tanh

        layer_list = list()
        for i in range(self.depth - 1):
            layer_list.append(
                    ("layer_%d" % i, nn.Linear(layers[i], layers[i+1]))
                    )
            layer_list.append(("activation_%d" % i, self.activation()))

        layer_list.append(
                ("layer_%d" % (self.depth - 1), nn.Linear(layers[-2], layers[-1]))
                )
        layerDict = OrderedDict(layer_list)
        self.layers = nn.Sequential(layerDict)

        print("Model for AllenCahn problem is initialized:\n", self.layers)

    def input_encoding(self, t, x):
        w = 2.0 * torch.pi / self.L
        k = torch.arange(1, self.M + 1, device=x.device) # [10]
        out = torch.cat([t, torch.ones_like(t), 
                         torch.cos(k * w * x), torch.sin(k * w * x)], dim=-1)
        return out
    
    def forward(self, t, x):
        out = self.input_encoding(t=t, x=x)
        out = self.layers(out)
        return out

    def compute_loss(self, hat_u, t, x):
        u_t = torch.autograd.grad(
                hat_u, t,
                grad_outputs=torch.ones_like(hat_u),
                retain_graph=True,
                create_graph=True,
                )[0]
        u_x = torch.autograd.grad(
                hat_u, x,
                grad_outputs=torch.ones_like(hat_u),
                retain_graph=True,
                create_graph=True,
                )[0]
        u_xx = torch.autograd.grad(
                u_x, t,
                grad_outputs=torch.ones_like(u_x),
                retain_graph=True,
                create_graph=True,
                )[0]
        self.res_pred = u_t + 5 * hat_u ** 3 - 5 * hat_u - 0.0001 * u_xx
        loss_res = torch.mean(self.res_pred ** 2)

        ics_pred = self.forward(self.data.T_0, self.data.X_0)
        loss_ics = torch.mean((self.data.Y_0 - ics_pred.flatten()) ** 2)

        return loss_res, loss_ics

    def evaluate(self):
        hat_u_star = self.forward(t=self.data.T_star, x=self.data.X_star)
        hat_u_star = hat_u_star.reshape(*self.data.usol.shape)
        l2_error = np.linalg.norm(hat_u_star - self.data.usol) / self.data.usol_norm
        return hat_u_star, l2_error

def init_allen_problem(datapath, device):
    """
    The function returns objects: data, model, optimizer, and scheduler,
    where parameters and choices of algorithms are exactly
    the same as ones used for experiments by authors [Daw et al 2023]
    """
    data = AllenCahnData(datapath, device) 
    model = DNNAllenCahn(data, max_iter=50000, iter_size=1000, batch_size=50).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-3)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=5000, gamma=0.9)
    return data, model, optimizer, scheduler
