import argparse
import json
import os
import sys
import time

import numpy as np
import pandas as pd
import ray
import torch

from tqdm import trange

from utils.misc import set_seed, linlog_spaced_int
from utils.models import init_allen_problem, init_imbalance_problem
from utils.samplers import init_sampler_from_args


# -------------------------------- ARGPARSE

parser = argparse.ArgumentParser(
    description="Parsing arguments for experiment setup",
)

# args to determine sampler method and problem to solve in experiment
parser.add_argument("sampler", choices=["breed", "r3", "uniform"], 
        help="""Choice of sampler method for expriment.
             breed: author method
             r3: Retain-Resample-Release (R3) Sampling [https://arxiv.org/abs/2207.02338]
             uniform: each iteration a train set is sampled uniformly in domain"""
             )

parser.add_argument("--problem", choices=["imbalance", "allencahn"], default="imbalance", help="Choice of problem to solve.")

# args about R value configuration
parser.add_argument("-p", "--poly", type=float,
        required=(sys.argv[1] == "breed"), default=0.,
        help="Parameter of polynomial degree for R-value scheme: (ev-sv)*x^p+sv, x=[0,1]=#epoch/|epochs|")
parser.add_argument("-sv", "--startval", type=float,
        required=(sys.argv[1] == "breed"), default=0.0,
        help="Parameter of start value for R-value scheme:(ev-sv)*x^p+sv, x=[0,1]=#epoch/|epochs|")
parser.add_argument("-ev", "--endval", type=float,
        required=(sys.argv[1] == "breed"), default=0.0,
        help="Parameter of end value for R-value scheme:(ev-sv)*x^p+sv, x=[0,1]=#epoch/|epochs|")
parser.add_argument("-ca", "--constafter", type=float,
        required=(sys.argv[1] == "breed"), default=0.0,
        help="Parameter of R-value scheme where first (constafter*epochs if < 1)/(constafter is >1) epochs scenario is given by parameters ev,sv,p end after it is constant at ev")

parser.add_argument("--sigma", type=float, default=0.01, help="Standard deviation parameter for gaussian neighbour sample")

# epoch sizes
parser.add_argument("--epochs", type=int, default=None)
parser.add_argument("--epochsize", type=int, default=None)
parser.add_argument("--batchsize", type=int, default=None)

# args for path to directories
parser.add_argument("-D", "--datadir", type=str, help="Directory with data for validation (input.npy output.npy or AC.mat).")
parser.add_argument("-O", "--outputdir", type=str, help="Directory for saving experiment: input_{epoch:02d}.npy, output_{epoch:02d}.npy, loss_{epoch:02d}.npy, predict_{epoch:02d}.npy, model_checkpoint_best.pth, (train_stats.csv).")

parser.add_argument("--seed", type=int, default=2023, help="Random initialization.")
args = parser.parse_args()

# --------------------------------- SETUP
print(f"--- GIVEN PATHS ---\nDatasets:\t{args.datadir}\nExperiments:\t{args.outputdir}")
if not os.path.isdir(args.outputdir):
    print("(Experiment directory was created)")
    os.makedirs(args.outputdir)

MODE_NAME = args.sampler 
USE_SNAKE = "snake" in args.outputdir
if USE_SNAKE:
    # because snake creates names and directory itself
    RUN_NAME = args.outputdir.split("/")[-1]
    EXP_PATH = args.outputdir
else:
    # because we want to store experiments in directory 01, 02, 03...
    RUN_NAME = max(
            (int(name) for name in os.listdir(args.outputdir) if os.path.isdir(os.path.join(args.outputdir, name))
                ), default=0) + 1
    RUN_NAME = "{:02d}".format(RUN_NAME)
    EXP_PATH = os.path.join(args.outputdir, RUN_NAME)

os.makedirs(EXP_PATH, exist_ok=True)
with open(f"{EXP_PATH}/clargs.json", "w") as args_file:
    json.dump(vars(args), args_file)

train_stats = pd.DataFrame(columns=["train_loss", "test_loss", "hard_test_loss", "misc"], dtype=float)

# --------------------------------- TRAINLOOP SETUP

set_seed(args.seed)

# CUDA support
if torch.cuda.is_available():
    raise NotImplementedError("The authors did not use GPU acceleration for experiments, code is not adapted. Feel free to open an issue or request a push.")
    device = torch.device(f"cuda:{args.gpu_id}")
else:
    device = torch.device("cpu")

# here we initialize data object, model object (it has already loss method inside)
# optimizer and scheduler, precise to problem to solve
# code is not generic to a greater extent intentionally by authors for an ease of reproduction
# however, each part (e.g., sampler algorithm) can be easiy copied and reused

if args.problem == "imbalance":
    data, model, optimizer, scheduler = init_imbalance_problem(args.datadir, device)
    # simulation of GD are parallelized with Ray
    ray.init(
        include_dashboard=False,
        ignore_reinit_error=True,
        )
elif args.problem == "allencahn":
    data, model, optimizer, scheduler = init_allen_problem(f"{args.datadir}/AC.mat", device)

epochs = args.epochs if args.epochs is not None else model.max_iter
epochsize = args.epochsize if args.epochsize is not None else model.iter_size
batchsize = args.batchsize if args.batchsize is not None else model.batch_size
if args.sampler == "r3":
    # according to paper
    batchsize = epochsize 
batches = epochsize // batchsize

# some saving settings
if epochs > 50:
    epochs_to_save = linlog_spaced_int(1, epochs, 50) - 1
    print("WARNING: Not all epochs data will be saved to economy space:\n", epochs_to_save)
else:
    epochs_to_save = np.arange(epochs)
id_to_save = 0

def save(flag, tensor, name, epoch):
    if flag:
        np.save(f"{EXP_PATH}/{name}_{epoch:07d}.npy", tensor.detach().numpy())

# value can be float for a "ratio of epoch" or int for a "number of epochs"
args.constafter = int(args.constafter * epochs) if args.constafter < 1 else int(args.constafter)
sampler = init_sampler_from_args(args, data.domain, epochsize, device)
# at this step, sampler already has a first uniform training set, accessed by sampler.current()

# --------------------------------- TRAIN LOOP
stats = {"train_loss": None, "test_loss": None, "hard_test_loss": None, "misc": None}
progressbar = trange(epochs)
for epoch in progressbar:
    is_epoch_to_save = (epoch == epochs_to_save[id_to_save])
    print(f"Do I save epoch {epoch}", is_epoch_to_save)
    if is_epoch_to_save:
        id_to_save += 1        
    samples = sampler.current().clone().detach().requires_grad_(True)
    if args.problem == "imbalance":
        # not data-free
        outputs = torch.from_numpy(data.run_simulation(samples.detach().numpy())).float().to(device)
    # shuffling for batching
    perm = np.random.permutation(epochsize)
    running_loss = 0
    for batch in range(batches):
        optimizer.zero_grad()
        perm_batch = perm[batch*batchsize:(batch + 1)*batchsize]
        if args.problem == "imbalance":
            # model loss is not data free
            x = samples[perm_batch].clone().detach().requires_grad_(True)
            y = outputs[perm_batch].clone().detach().requires_grad_(True)
            
            hat_y = model(x)
            # sampler gets loss value per sample (Huberloss)
            _, loss = model.compute_loss(hat_y, y)
        elif args.problem == "allencahn":
            t = samples[perm_batch,1:2].clone().detach().requires_grad_(True)
            x = samples[perm_batch,0:1].clone().detach().requires_grad_(True)

            hat_y = model(t, x)
            # model loss is data free (residuals)
            loss_res, loss_ics = model.compute_loss(hat_y, t, x)
            # sampler gets residual loss per sample
            loss = loss_res + loss_ics    
        loss.backward()
        optimizer.step()
        running_loss += loss.item()

    stats["train_loss"] = running_loss / batches

    # computing loss on the whole iteration set 
    # to get comparable loss stats per sample
    if args.problem == "imbalance":
        with torch.no_grad():
            loss_per_sample, _ = model.compute_loss(model(samples), outputs)
    elif args.problem == "allencahn":
        t = samples[:,1:2].clone().detach().requires_grad_(True)
        x = samples[:,0:1].clone().detach().requires_grad_(True)
        hat_y = model(t, x)
        _, _  = model.compute_loss(hat_y, t, x)
        loss_per_sample = model.res_pred

    save(is_epoch_to_save, sampler.current(), "samples", epoch)
    save(is_epoch_to_save, loss_per_sample, "loss_train", epoch)
    
    # loss should be non-negative
    sampler.update(torch.abs(loss_per_sample).detach())
    stats["misc"] = sampler.get_misc()
        
    with torch.no_grad():
        # validation
        if args.problem == "imbalance":
            hat_y = model(data.val_inputs)
            val_loss_per_sample, val_loss = model.compute_loss(hat_y, data.val_outputs)
            stats["test_loss"] = val_loss.item()
            save(is_epoch_to_save, val_loss_per_sample, "loss_test", epoch)
            save(is_epoch_to_save, hat_y, "predict", epoch)

            hard_hat_y = model(data.hard_val_inputs)
            hard_val_loss_per_sample, hard_val_loss = model.compute_loss(hard_hat_y, data.hard_val_outputs)
            stats["hard_test_loss"] = hard_val_loss.item()
            save(is_epoch_to_save, hard_val_loss_per_sample, "loss_hard_test", epoch)
            save(is_epoch_to_save, hard_hat_y, "hard_predict", epoch) 
            scheduler.step(val_loss) # it is Plateau
        elif args.problem == "allencahn":
            hat_u_star = model(t=data.T_star, x=data.X_star)
            hat_u_star = hat_u_star.reshape(*data.usol.shape)
            l2_error = np.linalg.norm(hat_u_star - data.usol) / data.usol_norm
            stats["test_loss"] = l2_error.mean()
            #save(is_epoch_to_save, l2_error, "loss_test", epoch)
            save(is_epoch_to_save, hat_u_star, "predict", epoch)
            scheduler.step() # it is Step

    train_stats.loc[epoch] = stats
    progressbar.set_postfix(stats)

    train_stats.to_csv(f"{EXP_PATH}/train_stats.csv")

if args.problem == "imbalance":
    ray.shutdown()
