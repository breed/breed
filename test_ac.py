import utils.datasets as d
import utils.models as m
import utils.samplers as s

import torch
from numpy.random import permutation

from utils.misc import set_seed

set_seed(1998)
device=None

datai, modeli1, optimizeri1, scheduleri1 = m.init_allen_problem("data/AC.mat", device)
datai, modeli2, optimizeri2, scheduleri2 = m.init_allen_problem("data/AC.mat", device)

modeli2.load_state_dict(modeli1.state_dict().copy())

N = 1000
lds = s.BreedSampler(datai.domain, 0.005, 0.15, 0.7, 4000, N)
r3s = s.R3Sampler(tuple(datai.domain[0]), tuple(datai.domain[1]), N)
r3s.x, r3s.t = lds.x[:,0:1].clone(), lds.x[:,1:2].clone()

print(lds.R)

batchsize = 100
nbatches = N // batchsize

print("LDS current:\n", lds.current()[[0,1,3,-3,-2,-1]])
print("R3S current:\n", r3s.current()[[0,1,3,-3,-2,-1]])
for i in range(10000):
    print(i, end=" ")
    perm = permutation(N)
    run_loss = 0
    for batch in range(nbatches):
        perm_batch = perm[batch*batchsize:(batch + 1)*batchsize]
        x = lds.current()[perm_batch,0:1].clone().requires_grad_(True)
        t = lds.current()[perm_batch,1:2].clone().requires_grad_(True)
        optimizeri1.zero_grad()
        hat_lds = modeli1(t, x)
        loss_res, loss_ic = modeli1.compute_loss(hat_lds, t, x)
        loss = loss_res + loss_ic
        run_loss += loss.item()
        loss.backward()
        optimizeri1.step()
    
    x = r3s.current()[:,0:1].clone().requires_grad_(True)
    t = r3s.current()[:,1:2].clone().requires_grad_(True)

    hat_r3s = modeli2(t, x)
    optimizeri2.zero_grad()
    loss_res, loss_ic = modeli2.compute_loss(hat_r3s, t, x)
    loss = loss_res + loss_ic
    loss.backward()
    optimizeri2.step()

    #print("LDS loss =", run_loss / nbatches , "\t", 
    #        "R3S loss =", loss.item())

    
    x = lds.current()[:,0:1].clone().requires_grad_(True)
    t = lds.current()[:,1:2].clone().requires_grad_(True)
    hat_lds = modeli1(t, x)
    modeli1.compute_loss(hat_lds, t, x)
    with torch.no_grad():
        lds.update(torch.abs(modeli1.res_pred).detach())
        r3s.update(torch.abs(modeli2.res_pred).detach())
        if not i % 100:
            print("\n--------")
            print("R=", lds.R[lds.R_i], "oob", lds.oob_count_global, "retained", r3s.n_retained[-1])
            print("LDS: val loss =", modeli1.evaluate()[1], "\t", 
                    "R3S: val loss =", modeli2.evaluate()[1])
            print("--------")

