# Breed: method to sample informative points for training simulation-based neural networks

The code presented in this repository provides reproducible experiments of the paper submission to workshop  **NeurIPS 2024 Machine Learning for Physical Sciences**.

To run experiments you need to install `nix` package manager. How to install: https://nixos.org/download

To run experiments, presented in paper, run the following command:

```
nix-shell
snakemake -c 1  
```

But we recommend to run experiments on some cluster with the following command (details: https://snakemake.readthedocs.io/en/stable/executing/cluster.html):

```
snakemake --cluster "<CLUSTER NODE SUBMISSION>" --jobs 30
```

If you cluster does not support Nix by default, you still need to install Nix.
But, there might be an issue with running Snakemake from Nix (collision of Python versions between Nix's and the cluster one).
In this case, we recommend installing Snakemake via `pip` and running the above command.

If you want to run just one experiment, or try other parameters, see ```python run.py --help```. 

The files description:

`Snakefile`:
    - in this project we are using ```snakemake``` framework for the reproducibility of experiments, as well as a tool for easy managing and running "grid"-experiments. More about `snakemake`: https://snakemake.github.io/

`flake.lock`, `flake.nix`:
    - in this project we are using Nix package manager, which provides full reproducibility. We are greatly encourage the research community to use Nix instead of "requirements.txt". A popularization of such a tool as a basic instrument in research experiments will bring a higher quality papers with code.

`run.py`:
    - script to run experiments

`utils/datasets.py`:
    - definition of two classes: ImbalancedData and AllenCahnData, which provides problems dataset object

`utils/gdsimulator.py`:
    - definition of functions for toy benchmark: Gradien Decsent on Imbalance pits surface

`utils/generate\_imbalance\_valset.py`:
    - script to create validation dataset for toy benchmark

`samplers.py`:
    - definition of two samplers classes: authors method sampler and  R3Sampler [https://arxiv.org/pdf/2207.02338.pdf]

`misc.py`:
    - miscellaneous functions
