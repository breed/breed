import time
NIX_COMMAND = "nix-shell --command "

SAMPLER_CHOICE = ["breed", "r3", "uniform"]
PROBLEM_CHOICE = [
                # "imbalance",
                 "allencahn"
                 ]

SEEDS = [42, 314159, 1998, 2023, 1618]
# seed is fixed for reproducibility of the script
# but seeds are common known numbers so no tricks

SIGMA = {
         "imbalance": 0.005,
         "allencahn": 0.001
        }

SV = {
         "imbalance": 0.15,
         "allencahn": 0.15
        }

EV = {
         "imbalance": 0.7,
         "allencahn": 0.7
        }

CA = {
         "imbalance": 25,
         "allencahn": 2000 
        }

EXP_PATH = "./snake/"
DATA_PATH = "./data/"

rule all:
    input:
        expand("{ppath}/{problem}/seed_{seed}_method_{sampler}",
                ppath=EXP_PATH,
                problem=PROBLEM_CHOICE,
                seed=SEEDS,
                sampler=SAMPLER_CHOICE)
rule experiment_run:
    input:
        "utils/samplers.py",
        "utils/models.py",
        "utils/datasets.py",
        "data/inputs.npy",
        "data/outputs.npy",
        "data/AC.mat",
        run = "run.py"
    output:
        directory("{ppath}/{problem}/seed_{seed}_method_{sampler}")
    params:
        sigma = lambda wildcards: SIGMA[wildcards.problem],
        sv = lambda wildcards: SV[wildcards.problem],
        ev = lambda wildcards: EV[wildcards.problem],
        ca = lambda wildcards: CA[wildcards.problem]
    shell:        
        ("{NIX_COMMAND} 'python3 {input.run} {wildcards.sampler} --problem {wildcards.problem} --seed {wildcards.seed}"
         " --sigma {params.sigma} -p 1 -sv {params.sv} -ev {params.ev} -ca {params.ca}"
         " --datadir {DATA_PATH} --outputdir {output}'"
         )

